import re


def checkName(fname) -> bool:
    return True if re.search("^[\sa-zA-Z-_.']+$", fname) else False
