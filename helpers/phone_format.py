import re


def getCompanyName(number):
    code = number.split(' ')[1]
    codes = {
        "99": "Uzmobile",
        "33": "Humans",
        "94": "Ucell",
        "93": "Ucell",
        "97": "Mobiuz",
        "91": "Beeline",
        "90": "Beeline"
    }
    return codes[code]


def getCountryName(number):
    code = number.split(' ')[0]
    codes = {
        "+998": "UZ",
        "+7": "RU"
    }
    return codes[code]


def checkFormat(number):
    try:
        company_name = getCompanyName(number)
        country_code = getCountryName(number)
        return True
    except Exception as e:
        return False


def validateNumber(number):
    pattern = "^(\+|.*)[0-9]{3}[0-9]{2}[0-9]{3}[0-9]{2}[0-9]{2}$"
    return True if re.search(pattern, number) else False
