from config.database import Database

class User(Database):

    def __init__(self):
        super().__init__()

    def findBy(self, column, value):
        self.db.commit()
        self.cursor.execute(f"SELECT * FROM customers WHERE {column} = {value}")
        return self.cursor.fetchone()

    def has(self, column, chat_id):
        self.db.commit()
        self.cursor.execute(f"SELECT {column} FROM customers WHERE telegram_chat_id = {chat_id}")
        data = self.cursor.fetchone()
        if data is not None and data.get(column) is not None:
            return True
        else:
            return False

    def update(self, condition, **params):
        set = ""
        items = params.items()
        lister = list(items)
        for column, value in items:
            if lister[-1][0] is column:
                set += f'{column} = "{value}"'
            else:
                set += f'{column} = "{value}", '

        self.cursor.execute(f"UPDATE customers SET {set} WHERE {condition}")
        self.db.commit()

    def delete(self, condition):
        self.cursor.execute(f"DELETE FROM customers WHERE {condition}")
        self.db.commit()


