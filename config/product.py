from config.database import Database

class Product(Database):

    IMAGE_FULL_PATH = "http://botadmin.loc/storage/products/images/"

    def __init__(self):
        super().__init__()

    def getProduct(self, product_id):
        query = f"SELECT p.id, p.name, p.description, p.price, p.currency, i.image FROM products p JOIN images i ON {product_id} = i.imageable_id WHERE p.id = {product_id}"
        self.cursor.execute(query)
        return self.cursor.fetchone()

    def getProducts(self):
        query = f"SELECT * FROM products"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def buyProduct(self, p_id, c_id):
        self.cursor.execute(f"SELECT id FROM `users` WHERE chat_id = {c_id}")
        user_id = self.cursor.fetchone()['id']
        query = f"INSERT INTO `product_user` (`user_id`, `product_id`) VALUES({user_id}, {p_id})"
        self.cursor.execute(query)
        self.db.commit()

    def getSoldProduct(self, p_id, c_id):
        self.db.commit()
        self.cursor.execute(f"SELECT id FROM `customers` WHERE telegram_chat_id = {c_id}")
        user_id = self.cursor.fetchone()
        self.cursor.execute(f"SELECT * FROM product_user WHERE product_id = {p_id} AND user_id = {user_id['id']}")
        if self.cursor.fetchone():
            return True
        else:
            return False

    def getPurchases(self, chat_id):
        query = f"SELECT p.id, p.name, p.description, p.price, p.currency, i.image FROM products p JOIN images i ON p.id = i.imageable_id JOIN product_user pu ON p.id = pu.product_id WHERE pu.user_id = {self.getUserId(chat_id)}"
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def getUserId(self, chat_id):
        self.cursor.execute(f"SELECT id FROM `customers` WHERE telegram_chat_id = {chat_id}")
        return self.cursor.fetchone()['id']
