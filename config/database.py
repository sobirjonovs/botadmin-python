# Database class
import mysql.connector
from mysql.connector import errorcode
import sys
import re

class Database:
    host = 'localhost'
    user = 'sanjarbek'
    password = 'password'
    database = 'botlar'
    port = '3306'
    cursor = None
    db = None

    def __init__(self):
        try:
            self.db = mysql.connector.connect(
                host=self.host,
                user=self.user,
                password=self.password,
                port=self.port,
                database=self.database
            )
            self.cursor = self.db.cursor(dictionary=True, buffered=True)

        except mysql.connector.Error as er:
            if er.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something went wrong with username or password")
                sys.exit()
            elif er.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database name is wrong")
                sys.exit()
            else:
                print(er)
                sys.exit()
    def createTable(self, name, columns):
        self.cursor.execute("CREATE TABLE {} ({})".format(name, ",".join(columns)))

    def createDatabase(self, name):
        self.cursor.execute("CREATE DATABASE {}".format(name))

    def all(self, table_name):
        self.cursor.execute("SELECT * FROM {}".format(table_name))
        return self.cursor.fetchall()

    def insertIn(self, table_name, columns, data):
        columns = ",".join(columns)
        values = ""
        i = 0
        for value in data:
            i = i + 1
            if i is len(data):
                values += f'"{value}"'
            else:
                values += f'"{value}",'

        sql = f"INSERT INTO {table_name}({columns}) VALUES({values})"
        self.cursor.execute(sql)

    def dropTable(self, table_name):
        self.cursor.execute(f"DROP TABLE {table_name}")

    def commit(self):
        self.db.commit()

    def getToken(self):
        self.cursor.execute('SELECT token FROM config')
        bot = self.cursor.fetchone()
        return bot['token']
