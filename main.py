import datetime
import urllib.request
import requests

from config.database import Database as DB
from config.product import Product
from config.user import User
from helpers.helpers import chunkMenu
from helpers.name_checker import checkName
from helpers.phone_format import validateNumber
from telegram import Update, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton
from telegram.ext import (Updater, CommandHandler, MessageHandler,
                          Filters, CallbackContext, ConversationHandler,
                          CallbackQueryHandler)

db = DB()
user = User()
prod = Product()

# Initial actions
STATE_START_ACTIONS = 0
STATE_REGISTER = 1
STATE_NAME = 3
STATE_PHONE = 4
STATE_START = 5

# Shopping actions
STATE_PRODUCT = 1
STATE_PRODUCTS = 2
STATE_ACTIONS = 3
STATE_PHOTO = 6

buttons = InlineKeyboardMarkup([
    [
        InlineKeyboardButton("👤 Profilim", callback_data="profile"),
        InlineKeyboardButton("🛒 Xaridlarim", callback_data="purchases")
    ],
    [
        InlineKeyboardButton("👜 Mahsulotlar", callback_data="myproducts")
    ]
])


def start(update: Update, context: CallbackContext) -> None:
    name = update.message.from_user.full_name
    chat_id = update.message.from_user.id
    if user.has('telegram_chat_id', chat_id) is False:
        db.insertIn('customers', ['telegram_chat_id', 'created_at'], [str(chat_id), datetime.datetime.now()])
        db.commit()

    update.message.reply_html(
        text=f"😊 <b>Assalomu alaykum, {name}!</b>. Xush kelibsiz!",
        reply_markup=buttons
    )

    return STATE_START_ACTIONS


def register(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    chat_id = query.message.chat.id
    if not user.has('phone', chat_id):
        contact = KeyboardButton(text="📞 Telefon nomerni jo'natish", request_contact=True)

        query.message.reply_html(
            text="📞Telefon raqamingizni yuboring",
            reply_markup=ReplyKeyboardMarkup([[contact]], resize_keyboard=True, one_time_keyboard=True))

        return STATE_PHONE
    else:
        query.message.reply_text(text="❌ Siz oldin ro'yxatdan o'tgansiz. Qayta ro'yxatdan o'tish mumkin emas.")


# def name(update: Update, context: CallbackContext):
#     namer = update.message.text
#     chat_id = update.message.from_user.id
#     if checkName(namer) is True:
#         user.update(f"chat_id = {chat_id}", name=namer)
#         db.commit()
#         contact = KeyboardButton(text="📞 Telefon nomerni jo'natish", request_contact=True)
#
#         update.message.reply_html(
#             text="📞Telefon raqamingizni yuboring",
#             reply_markup=ReplyKeyboardMarkup([[contact]], resize_keyboard=True, one_time_keyboard=True))
#
#         return STATE_PHONE
#     else:
#         button = InlineKeyboardMarkup([[InlineKeyboardButton("*️⃣ Ro'yxatdan o'tish", callback_data="register")]])
#         update.message.reply_text("❌ Ma'lumotda xatolik mavjud", reply_markup=button)
#         return STATE_REGISTER


def phone(update: Update, context: CallbackContext):
    contact = update.effective_message.contact
    phone = contact.phone_number
    chat_id = update.message.from_user.id
    if validateNumber(phone):
        user.update(f'telegram_chat_id = {chat_id}', phone=phone)
        update.message.reply_text(
            "<b>✅ Ma'lumotlaringiz muvaffaqiyatli saqlandi.</b>\n\n Endi botdan bemalol foydalanishingiz mumkin. Botni to'liq yuklab olish uchun /start buyrug'ini bering.",
            parse_mode='HTML')
        return STATE_START
    else:
        update.message.reply_text("❌ Noto'g'ri ma'lumot kiritildi, qaytadan to'g'ri nomerni kiriting.")
        return STATE_PHONE


def myProfile(update: Update, context: CallbackContext):
    update = update.callback_query
    chat_id = update.from_user.id
    data = user.findBy('telegram_chat_id', chat_id)
    link_to_profile = '<a href="tg://user?id=' + str(data['telegram_chat_id']) + '">' + str(data['telegram_chat_id']) + '</a>'
    name = data['name'] if data['name'] is not None else update.from_user.first_name
    text = f"<b>✅ Mening ma'lumotlarim</b>\n\n<b>👤 F.I.Sh:</b> {name}\n<b>📞 Telefon raqamim:</b> <code>{data['phone']}</code>\n<b>🆔 ID:</b> {link_to_profile}"
    update.message.delete()
    update.message.reply_text(
        text=text,
        parse_mode='HTML',
        reply_markup=buttons
    )

    return STATE_START_ACTIONS


def buyProduct(update, context):
    callback = update.callback_query
    chat_id = callback.from_user.id
    if user.has('phone', chat_id):
        product_id = callback.data.split('_')[2]
        callback.message.delete()
        product = prod.getProduct(product_id)
        text = f"<b>🛍 Sotib olish bo'limi</b>\n\n {product['name']}ni sotib olish uchun <code> 8600 0605 0506 8505</code> karta raqamiga <b>{product['price']} {product['currency']}</b> summani o'tkazing. O'tkazib bo'lgandan so'ng, chek rasmini jo'nating. \n\n ⚠️ Favqulodda holat bo'lganda, @supportbotadmin ga murojaat qilishingizni so'raymiz."
        callback.message.reply_text(text=text, parse_mode='HTML')

        return STATE_PHOTO
    else:
        button = InlineKeyboardMarkup([[InlineKeyboardButton("*️⃣ Ro'yxatdan o'tish", callback_data="register")]])
        callback.message.delete()
        callback.message.reply_html(
            text="<b>😉 Assalomu alaykum, {}!</b> \n\n☹Afsuski botdan ro'yxatdan o'tmaganligingiz uchun uning to'liq imkoniyatlaridan foydalana olmaysiz.️\n\n🤓 Lekin hali kech emas, ro'yxatdan o'tish uchun shunchaki quyidagi <b>*️⃣ Ro'yxatdan o'tish</b> tugmasini bossangiz kifoya.".format(
                callback.from_user.first_name),
            reply_markup=button)
        return STATE_REGISTER


def getProduct(update, context):
    query = update.callback_query
    id = query.data.split('_')[1]
    product = prod.getProduct(id)
    photo = f"{Product.IMAGE_FULL_PATH}{product['image']}"
    buy_button = InlineKeyboardButton("🛒 Sotib olish", callback_data=f"buy_product_{id}")
    back_button = InlineKeyboardButton("🔙 Orqaga", callback_data="back")
    if not prod.getSoldProduct(product['id'], query.from_user.id):
        caption = f"<b>🗳 Mahsulot nomi: </b> {product['name']}\n<b>📋 Mahsulot haqida: </b>{product['description']}\n<b>💴 Narxi: </b>{product['price']} {product['currency']}\n\n<b>❌ Sotib olinmagan</b>"
        buttons = InlineKeyboardMarkup([[buy_button, back_button]])
    else:
        caption = f"<b>🗳 Mahsulot nomi: </b> {product['name']}\n<b>📋 Mahsulot haqida: </b>{product['description']}\n<b>💴 Narxi: </b>{product['price']} {product['currency']}\n\n<b>✅ Sotib olgansiz</b>"
        buttons = InlineKeyboardMarkup([[back_button]])

    with urllib.request.urlopen(photo) as remote:
        query.message.delete()
        query.message.reply_photo(
            photo=remote.read(),
            caption=caption,
            parse_mode='HTML',
            reply_markup=buttons
        )

    return STATE_START_ACTIONS


def getPurchases(update, context):
    update = update.callback_query
    if user.findBy('telegram_chat_id', update.from_user.id):
        purchases = prod.getPurchases(update.from_user.id)
        if purchases:
            buttons = []
            for chunks in list(chunkMenu(purchases, 2)):
                button = []
                for product in chunks:
                    button.append(InlineKeyboardButton(product['name'], callback_data=f"product_{product['id']}"))
                buttons.append(button)

            inline_buttons = InlineKeyboardMarkup(buttons)
            update.message.delete()
            update.message.reply_text(
                text="<b>📋 Ushbu ro'yxatdan o'zingizga kerakli bo'lgan mahsulotni tanlang 👇</b>",
                reply_markup=inline_buttons,
                parse_mode='HTML'
            )

            return STATE_START_ACTIONS
        else:
            update.message.reply_text(
                text="⚠️ Afsuski hali hech qanday mahsulot xarid qilmagansiz",
                parse_mode='HTML'
            )
    else:
        button = InlineKeyboardMarkup([[InlineKeyboardButton("*️⃣ Ro'yxatdan o'tish", callback_data="register")]])
        update.message.reply_html(
            text="<b>😉 Assalomu alaykum, {}!</b> \n\n☹Afsuski botdan ro'yxatdan o'tmaganligingiz uchun uning to'liq imkoniyatlaridan foydalana olmaysiz.️\n\n🤓 Lekin hali kech emas, ro'yxatdan o'tish uchun shunchaki quyidagi <b>*️⃣ Ro'yxatdan o'tish</b> tugmasini bossangiz kifoya.".format(
                name),
            reply_markup=button)
        return STATE_REGISTER


def getProducts(update, context):
    update = update.callback_query
    if user.findBy('telegram_chat_id', update.from_user.id):
        products = prod.getProducts()
        buttons = []
        for chunks in list(chunkMenu(products, 2)):
            button = []
            for product in chunks:
                button.append(InlineKeyboardButton(product['name'], callback_data=f"product_{product['id']}"))
            buttons.append(button)

        inline_buttons = InlineKeyboardMarkup(buttons)
        update.message.delete()
        update.message.reply_text(
            text="<b>📋 Ushbu ro'yxatdan o'zingizga kerakli bo'lgan mahsulotni tanlang 👇</b>",
            reply_markup=inline_buttons,
            parse_mode='HTML'
        )

        return STATE_START_ACTIONS
    else:
        button = InlineKeyboardMarkup([[InlineKeyboardButton("*️⃣ Ro'yxatdan o'tish", callback_data="register")]])
        update.message.reply_html(
            text="<b>😉 Assalomu alaykum, {}!</b> \n\n☹Afsuski botdan ro'yxatdan o'tmaganligingiz uchun uning to'liq imkoniyatlaridan foydalana olmaysiz.️\n\n🤓 Lekin hali kech emas, ro'yxatdan o'tish uchun shunchaki quyidagi <b>*️⃣ Ro'yxatdan o'tish</b> tugmasini bossangiz kifoya.".format(
                name),
            reply_markup=button)
        return STATE_REGISTER


def getPhoto(update, context):
    file_id = update.message.photo[-1].file_id
    newFile = context.bot.getFile(file_id)
    payload = {"image": newFile.file_path}
    r = requests.post("http://botadmin.loc/api/image", data=payload)
    if r.status_code == 200:
        update.message.reply_text("✅ Barcha ma'lumotlar adminga yuborildi. Iltimos kuting.")
    else:
        update.message.reply_text("❌ Xatolik yuzaga keldi qaytadan urinib ko'ring.")


def main():
    updater = Updater(db.getToken(), use_context=True)
    dispatcher = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            STATE_START_ACTIONS: [
                CallbackQueryHandler(myProfile, pattern="^profile$"),
                CallbackQueryHandler(getProducts, pattern="^myproducts$"),
                CallbackQueryHandler(getPurchases, pattern="^purchases"),
                CallbackQueryHandler(getProduct, pattern="^product_[0-9]*$"),
                CallbackQueryHandler(getProducts, pattern="^back$"),
                CallbackQueryHandler(buyProduct, pattern="^buy_product_[0-9]+$"),
            ],
            STATE_REGISTER: [CallbackQueryHandler(register)],
            # STATE_NAME: [MessageHandler(Filters.text, name)],
            STATE_PHONE: [MessageHandler(Filters.contact, phone)],
            STATE_PHOTO: [MessageHandler(Filters.photo, getPhoto)],
            # STATE_START: [CommandHandler("start", start)]
        },
        fallbacks=[CommandHandler('start', start)]
    )

    dispatcher.add_handler(conv_handler)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
