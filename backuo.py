from telegram import Update, ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import (Updater, CommandHandler, MessageHandler,
                          Filters, CallbackContext, ConversationHandler,
                          CallbackQueryHandler)
from config.database import Database as DB
from config.user import User
from config.product import Product
from helpers.name_checker import checkName
from helpers.phone_format import validateNumber, getCountryName, getCompanyName
import urllib.request
import re
from helpers.helpers import chunkMenu

db = DB()
user = User()
prod = Product()

# Initial actions
STATE_START_ACTIONS = 0
STATE_REGISTER = 1
STATE_AGREEMENT = 2
STATE_NAME = 3
STATE_PHONE = 4
STATE_START = 5

# Shopping actions
STATE_PRODUCT = 1
STATE_PRODUCTS = 2
STATE_ACTIONS = 3


def start(update: Update, context: CallbackContext) -> None:
    name = update.message.from_user.full_name
    chat_id = update.message.from_user.id
    if user.findBy('chat_id', chat_id):
        buttons = InlineKeyboardMarkup([
            [
                InlineKeyboardButton("👤 Profilim", callback_data="profile"),
                InlineKeyboardButton("🛒 Xaridlarim", callback_data="purchases")
            ],
            [
                InlineKeyboardButton("👜 Mahsulotlar", callback_data="products")
            ]
        ])
        update.message.reply_html(
            text=f"😊 <b>Assalomu alaykum, {name}!</b>. Xush kelibsiz!",
            reply_markup=buttons
        )

        return STATE_START_ACTIONS
    else:
        button = InlineKeyboardMarkup([[InlineKeyboardButton("*️⃣ Ro'yxatdan o'tish", callback_data="register")]])
        update.message.reply_html(
            text="<b>😉 Assalomu alaykum, {}!</b> \n\n☹Afsuski botdan ro'yxatdan o'tmaganligingiz uchun uning to'liq imkoniyatlaridan foydalana olmaysiz.️\n\n🤓 Lekin hali kech emas, ro'yxatdan o'tish uchun shunchaki quyidagi <b>*️⃣ Ro'yxatdan o'tish</b> tugmasini bossangiz kifoya.".format(
                name),
            reply_markup=button)
        return STATE_REGISTER


def echo(update: Update, context: CallbackContext) -> None:
    message = update.message.text
    update.message.reply_text(text="O'zingiz <b>{}siz :)</b>".format(message), parse_mode='HTML')


def getUsers(update: Update, context: CallbackContext) -> None:
    rows = db.all('users')
    result = ""
    for user in rows:
        result += f"ID: {user['id']}, Ism: {user['name']} \n"

    update.message.reply_text(text=result)


def register(update: Update, context: CallbackContext) -> int:
    query = update.callback_query
    name = query.message.chat.first_name
    chat_id = query.message.chat.id
    if not user.findBy('chat_id', chat_id):
        message = f"<b>😊 Assalomu alaykum, {name}! </b> \n\nBotimizdan ro'yxatdan o'tayotganingiz uchun tashakkur. Keyingi qadam juda oddiy. Shunchaki biz so'ragan ma'lumotlarni yuborasiz.\n\n<b>🤔 Ro'yxatdan o'tishning afzalligi nima?</b>\n\nAfzalligi palonchi-pistonchilardan iborat....\n\n<b>🤔Qaysi ma'lumotlarim kerak bo'ladi?</b>\n\n1. Telefon raqamingiz\n2. To'liq ismingiz. \n\nXo'sh, ushbu ma'lumotlarni berishga rozimisiz?"
        buttons = ReplyKeyboardMarkup([
            [
                "👍 Ha", "👎 Yo'q"
            ],
        ], resize_keyboard=True, one_time_keyboard=True)
        query.message.reply_text(text=message, parse_mode='HTML', reply_markup=buttons)
        return STATE_AGREEMENT
    else:
        update.message.reply_text(text="❌ Siz oldin ro'yxatdan o'tgansiz. Qayta ro'yxatdan o'tish mumkin emas.")


def yes(update: Update, context: CallbackContext) -> int:
    update.message.reply_text(
        text="<b>👍 Ajoyib. Bizga ishonganingiz uchun rahmat</b>\n\n Iltimos to'liq ism sharifingizni yozib yuboring<i>(masalan: Telegram Telegrambekov Pavel o'g'li)</i>. ",
        parse_mode='HTML')
    return STATE_NAME


def no(update: Update, context: CallbackContext) -> int:
    update.message.reply_text('Omadingizni bersin...')
    return STATE_START


def name(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id
    name = update.message.text
    if checkName(name):
        db.insertIn('users', ['name', 'chat_id'], [name, str(chat_id)])
        db.commit()
        update.message.reply_html(
            text="📞Telefon raqamingizni kiriting:\n\n<b>Raqamni kiritish qoidasiga amal qiling. Masalan:</b><code>+998 99 999-99-99</code>")
        return STATE_PHONE
    else:
        update.message.reply_text("❌ Ma'lumotda xatolik mavjud", reply_markup=InlineKeyboardMarkup(buttons))
        return STATE_REGISTER


def phone(update: Update, context: CallbackContext) -> int:
    phone = update.message.text
    chat_id = update.message.from_user.id
    if validateNumber(phone):
        user.update(f'chat_id = {chat_id}',
                    phone=phone,
                    phone_company=getCompanyName(phone),
                    phone_country=getCountryName(phone))
        update.message.reply_text(
            "<b>✅ Ma'lumotlaringiz muvaffaqiyatli saqlandi.</b>\n\n Endi botdan bemalol foydalanishingiz mumkin. Botni to'liq yuklab olish uchun /start buyrug'ini bering.",
            parse_mode='HTML')
        return STATE_START
    else:
        update.message.reply_text("❌ Noto'g'ri ma'lumot kiritildi, qaytadan to'g'ri nomerni kiriting.")
        return STATE_PHONE


def myProfile(update: Update, context: CallbackContext):
    chat_id = update.message.from_user.id
    data = user.findBy('chat_id', chat_id)
    link_to_profile = '<a href="tg://user?id=' + str(data['chat_id']) + '">' + str(data['chat_id']) + '</a>'
    text = f"<b>✅ Mening ma'lumotlarim</b>\n\n<b>👤 F.I.Sh:</b> {data['name']}\n<b>📞 Telefon raqamim:</b> <code>{data['phone']}</code>\n<b>🖥 Kompaniya nomi:</b> {data['phone_company']}\n<b>🌐 Mamlakat:</b> {data['phone_country']}\n<b>🆔 ID:</b> {link_to_profile}"
    update.message.reply_text(
        text=text,
        parse_mode='HTML'
    )


# def product(update, context):
#     product = prod.getProduct(1)
#     caption = f"<b>🗳 Mahsulot nomi: </b> {product['name']}\n<b>📋 Mahsulot haqida: </b>{product['description']}\n<b>💴 Narxi: </b>{product['price']} {product['currency']}"
#     photo = f"{Product.IMAGE_FULL_PATH}{product['image']}"
#     with urllib.request.urlopen(photo) as remote:
#         update.message.reply_photo(
#             photo=remote.read(),
#             caption=caption,
#             parse_mode='HTML'
#             )

def buyProduct(update, context):
    callback = update.callback_query
    chat_id = callback.from_user.id
    product_id = callback.data.split('_')[2]
    prod.buyProduct(product_id, chat_id)
    callback.message.delete()
    callback.message.reply_text(text="✅ Muvaffaqiyatli sotib oldingiz!")


def getProduct(update, context):
    query = update.callback_query
    id = query.data.split('_')[1]
    product = prod.getProduct(id)
    caption = f"<b>🗳 Mahsulot nomi: </b> {product['name']}\n<b>📋 Mahsulot haqida: </b>{product['description']}\n<b>💴 Narxi: </b>{product['price']} {product['currency']}"
    photo = f"{Product.IMAGE_FULL_PATH}{product['image']}"
    buy_button = InlineKeyboardButton("🛒 Sotib olish", callback_data=f"buy_product_{id}")
    back_button = InlineKeyboardButton("🔙 Orqaga", callback_data="back")
    with urllib.request.urlopen(photo) as remote:
        query.message.delete()
        query.message.reply_photo(
            photo=remote.read(),
            caption=caption,
            parse_mode='HTML',
            reply_markup=InlineKeyboardMarkup([[buy_button, back_button]])
        )

    return STATE_ACTIONS


def getProducts(update, context):
    products = prod.getProducts()
    buttons = []
    for chunks in list(chunkMenu(products, 2)):
        button = []
        for product in chunks:
            button.append(InlineKeyboardButton(product['name'], callback_data=f"product_{product['id']}"))
        buttons.append(button)

    inline_buttons = InlineKeyboardMarkup(buttons)

    update.message.reply_text(
        text="<b>📋 Ushbu ro'yxatdan o'zingizga kerakli bo'lgan mahsulotni tanlang 👇</b>",
        reply_markup=inline_buttons,
        parse_mode='HTML'
    )

    return STATE_PRODUCT


def main():
    updater = Updater(db.getToken(), use_context=True)
    dispatcher = updater.dispatcher
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            STATE_START_ACTIONS: [
                    CallbackQueryHandler(myProfile, pattern="^profile$")
            ],
            STATE_REGISTER: [CallbackQueryHandler(register)],
            STATE_AGREEMENT: [MessageHandler(Filters.regex('^👍 Ha$'), yes)],
            STATE_NAME: [MessageHandler(Filters.text, name)],
            STATE_PHONE: [MessageHandler(Filters.text, phone)],
            STATE_START: [CommandHandler("start", start)]
        },
        fallbacks=[CommandHandler('start', start)]
    )

    dispatcher.add_handler(conv_handler)

    sale_handler = ConversationHandler(
        entry_points=[MessageHandler(Filters.regex("^👜 Mahsulotlar$"), getProducts)],
        states={
            STATE_PRODUCT: [CallbackQueryHandler(getProduct)],
            STATE_PRODUCTS: [CallbackQueryHandler(getProducts)],
            STATE_ACTIONS: [
                CallbackQueryHandler(buyProduct, pattern="^buy_product_[0-9]+$"),
                MessageHandler(Filters.regex("^back$"), getProducts)
            ]
        },
        fallbacks=[MessageHandler(Filters.regex("^👜 Mahsulotlar$"), getProducts)]
    )
    dispatcher.add_handler(sale_handler)
    dispatcher.add_handler(MessageHandler(Filters.regex("^👎 Yo'q$"), no))
    dispatcher.add_handler(MessageHandler(Filters.regex("^👤 Profilim$"), myProfile))

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
